package job;

import javafx.application.Platform;
import javafx.scene.control.Alert;

import java.util.TimerTask;

public class ShowNotificationTask extends TimerTask {
    private Alert alert;

    public ShowNotificationTask(Alert alert) {
        this.alert = alert;
    }

    @Override
    public void run() {
        Platform.runLater(() -> alert.showAndWait());
    }
}
