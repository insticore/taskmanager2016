package controller;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import jfxtras.labs.scene.control.BigDecimalField;
import model.Task;
import model.TaskAdapter;
import org.apache.log4j.Logger;

import java.text.ParseException;
import java.util.Date;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * Created by Cyril on 22/01/2017.
 */
public class TaskController {
    private final static Logger logger = Logger.getLogger(TaskController.class.getName());
    @FXML
    private TextField titleField;
    @FXML
    private Button cancelButton;
    @FXML
    private TextField startField;
    @FXML
    private TextField endField;
    @FXML
    private CheckBox activeCheck;
    @FXML
    private CheckBox repeatCheck;
    @FXML
    private HBox repeatBox;
    @FXML
    private TaskAdapter task;
    @FXML
    private AnchorPane endBox;

    private BigDecimalField daysField = Common.getDecimalFieldInstance(0, 999999999);
    private BigDecimalField hoursField = Common.getDecimalFieldInstance(0, 23);
    private BigDecimalField minutesField = Common.getDecimalFieldInstance(0, 59);
    private BigDecimalField secondsField = Common.getDecimalFieldInstance(0, 59);

    private Stage stage;

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
        repeatBox.getChildren().add(daysField);
        repeatBox.getChildren().add(new Label("days"));
        repeatBox.getChildren().add(hoursField);
        repeatBox.getChildren().add(new Label("hours"));
        repeatBox.getChildren().add(minutesField);
        repeatBox.getChildren().add(new Label("minutes"));
        repeatBox.getChildren().add(secondsField);
        repeatBox.getChildren().add(new Label("seconds"));
        Platform.runLater(() -> cancelButton.requestFocus());
    }

    public void setTask(TaskAdapter task) {
        if (task == null) {
            clear();
            return;
        }
        this.task = task;
        titleField.setText(task.getTitle());
        startField.setText(task.getStart());
        endField.setText(task.getEnd());
        activeCheck.setSelected(task.getActive());
        repeatCheck.setSelected(task.isRepeated());
        repeatBox.setVisible(repeatCheck.isSelected());
        endBox.setVisible(repeatCheck.isSelected());
        setRepeatFields();
    }



    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    public void handleRepeated(){
        repeatBox.setVisible(repeatCheck.isSelected());
        endBox.setVisible(repeatCheck.isSelected());
    }

    @FXML
    public void handleCancel(){
        stage.close();
    }

    @FXML
    public void handleSave(){
        try {
            Date now = new Date();
            Date startTime = Task.DATE_FORMAT.parse(startField.getText());
            int repeat = getRepeatTime();
            Date endTime;
            if (repeat > 0)
                endTime = Task.DATE_FORMAT.parse(endField.getText());
            else
                endTime = startTime;
            if (startTime.compareTo(now) <= 0 || endTime.compareTo(now) <=0) {
                Common.showError("Tasks for the past are not supported!");
                logger.error("Tasks for the past are not supported!");
                return;
            }
            String title = titleField.getText();
            boolean active = activeCheck.isSelected();
            saveTask(task, title, startTime, endTime, repeat, active);
            stage.close();
        } catch (ParseException e) {
            Common.showError("No date specified.");
            logger.error(e.getMessage() + " - probably empty");
        } catch (IllegalArgumentException e) {
            Common.showError(e.getMessage());
            logger.error(e.getMessage());
        }
    }

    public int getRepeatTime(){
        if (!repeatCheck.isSelected())
            return 0;
        long repeat = TimeUnit.DAYS.toSeconds(Integer.parseInt(daysField.getText()));
        repeat += TimeUnit.HOURS.toSeconds(Integer.parseInt(hoursField.getText()));
        repeat += TimeUnit.MINUTES.toSeconds(Integer.parseInt(minutesField.getText()));
        repeat += Integer.parseInt(secondsField.getText());
        return (int) repeat;
    }

    public void setRepeatFields(){
        int[] intervals = task.getTask().getRepeatIntervals();
        daysField.setText(String.valueOf(intervals[0]));
        hoursField.setText(String.valueOf(intervals[1]));
        minutesField.setText(String.valueOf(intervals[2]));
        secondsField.setText(String.valueOf(intervals[3]));
    }

    @FXML
    public void startTimePicker(){
        Date time;
        if (task == null)
            time = new Date();
        else
            time = task.getTask().getStartTime();
       Common.showCalendarPicker(time, (ov, t, t1) -> {
           if (t1 == null)
               return;
           startField.setText(Task.DATE_FORMAT.format(t1.getTime()));
       });
    }
    @FXML
    public void endTimePicker(){
        Date time;
        if (task == null)
            time = new Date();
        else
            time = task.getTask().getEndTime();
        Common.showCalendarPicker(time, (ov, t, t1) -> {
            if (t1 == null)
                return;
            endField.setText(Task.DATE_FORMAT.format(t1.getTime()));
        });
    }

    private void saveTask(TaskAdapter task, String title, Date startTime, Date endTime, int repeat, boolean active) {
        if (task == null) {
            Main.getController().addTask(title, startTime, endTime, repeat, active);
        } else {
            task.set(title, startTime, endTime, repeat, activeCheck.isSelected());
        }
        if (Main.getController().calendarIsShown()){
            Main.getController().showCalendarWindow();
        }
    }

    private void clear(){
        titleField.setText("");
        startField.setText("");
        endField.setText("");
        activeCheck.setSelected(false);
        repeatCheck.setSelected(false);
        repeatBox.setVisible(repeatCheck.isSelected());
        endBox.setVisible(repeatCheck.isSelected());
        daysField.setText("0");
        hoursField.setText("0");
        minutesField.setText("0");
        secondsField.setText("0");
    }

    @FXML
    public void handleRemove(){
        showRemoveDialog();
    }

    private void showRemoveDialog(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog");
        alert.setHeaderText(null);
        alert.setContentText("Are you sure about that?");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            Main.getController().removeTask(task);
            stage.close();
        } else {
            alert.close();
        }
    }
}


