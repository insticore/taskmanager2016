package controller;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.stage.Stage;
import javafx.util.Callback;
import model.DateTaskEntry;
import model.Task;
import model.TaskAdapter;

import java.util.Date;
import java.util.Iterator;

public class CalendarController {
    @FXML
    private TableView<DateTaskEntry> tasksTable;
    @FXML
    private TableColumn<DateTaskEntry, String> dateColumn;
    @FXML
    private TableColumn<DateTaskEntry, String> titleColumn;

    private TasksListController tasksListController;
    private Stage stage;


    private ObservableList<DateTaskEntry> tasks = FXCollections.observableArrayList();

    public CalendarController() {
    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
        dateColumn.setCellValueFactory(cellData -> new SimpleObjectProperty<>(
                Task.DATE_FORMAT.format(cellData.getValue().getDate()))
        );
        titleColumn.setCellValueFactory(cellData -> cellData.getValue().getTaskAdapter().titleProperty());

        tasksTable.setItems(tasks);
        tasksTable.setRowFactory(tv -> {
            TableRow<DateTaskEntry> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (! row.isEmpty() && event.getButton()== MouseButton.PRIMARY
                        && event.getClickCount() == 1) {
                    TaskAdapter clickedRow = row.getItem().getTaskAdapter();
                    tasksListController.showTaskWindow(clickedRow);
                }
            });
            return row ;
        });
    }


    public void set(TasksListController tasksListController, ObservableList<TaskAdapter> tasks, Date start, Date end) {
        this.tasksListController = tasksListController;
        this.tasks.clear();
        this.tasks.addAll(TaskAdapter.calendar(tasks, start, end));
    }


    public void setStage(Stage stage) {
        this.stage = stage;
    }
}
