package controller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import jfxtras.labs.scene.control.BigDecimalField;
import jfxtras.scene.control.CalendarPicker;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Cyril on 28/01/2017.
 */
public class Common {
    public static void showError(String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }

    public static BigDecimalField getDecimalFieldInstance(int minValue, int maxValue) {
        NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
        nf.setMaximumFractionDigits(0);
        nf.setMinimumFractionDigits(0);
        BigDecimalField bigDecimalField =  new BigDecimalField(new BigDecimal(0), new BigDecimal(1), nf);
        bigDecimalField.setMinValue(new BigDecimal(minValue));
        bigDecimalField.setMaxValue(new BigDecimal(maxValue));
        bigDecimalField.setMaxHeight(13);
        bigDecimalField.setMaxWidth(68);
        return bigDecimalField;
    }

    public static void showCalendarPicker(Date initTime, ChangeListener<Calendar> listener) {
        CalendarPicker dateTime = new CalendarPicker();
        Calendar calendar = Calendar.getInstance();
        if (initTime != null)
            calendar.setTime(initTime);
        dateTime.withCalendar(calendar);
        dateTime.withShowTime(Boolean.TRUE);
        dateTime.withLocale(Locale.ENGLISH);
        dateTime.calendarProperty().addListener(listener);

        StackPane root = new StackPane();
        root.getChildren().add(dateTime);
        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.setTitle("Date & Time from JFXtras 2.2");
        stage.setScene(scene);
        stage.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!newValue) {
                stage.close();
            }
        });
        stage.show();
    }
}
