package controller;

import db.DataBase;
import javafx.application.Platform;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.util.Callback;
import model.Task;
import model.TaskAdapter;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;

public class TasksListController {
    @FXML
    private TableView<TaskAdapter> tasksTable;
    @FXML
    private TableColumn<TaskAdapter, String> titleColumn;
    @FXML
    private TableColumn<TaskAdapter, String> startColumn;
    @FXML
    private TableColumn<TaskAdapter, String> endColumn;
    @FXML
    private TableColumn<TaskAdapter, String> repeatColumn;
    @FXML
    private TableColumn<TaskAdapter, Boolean> activeColumn;
    @FXML
    private TextField startField;
    @FXML
    private TextField endField;

    private TaskController taskController;
    private Stage taskStage;
    private CalendarController calendarController;
    private Stage calendarStage;

    private ObservableList<TaskAdapter> tasks;

    // Reference to the main application.
    private Main main;

    public TasksListController() {   }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
        titleColumn.setCellValueFactory(cellData -> cellData.getValue().titleProperty());
        titleColumn.setCellValueFactory(cellData -> cellData.getValue().titleProperty());
        activeColumn.setCellValueFactory(cellData -> cellData.getValue().activeProperty());
        final Callback<TableColumn<TaskAdapter, Boolean>, TableCell<TaskAdapter, Boolean>> cellFactory = CheckBoxTableCell.forTableColumn(activeColumn);
        activeColumn.setCellFactory(column -> {
            TableCell<TaskAdapter, Boolean> cell = cellFactory.call(column);
            cell.setAlignment(Pos.CENTER);
            return cell;
        });
        activeColumn.setEditable(false);
        startColumn.setCellValueFactory(cellData -> cellData.getValue().startProperty());
        startColumn.setMinWidth(145);
        endColumn.setCellValueFactory(cellData -> cellData.getValue().endProperty());
        endColumn.setMinWidth(145);
        repeatColumn.setCellValueFactory(cellData -> cellData.getValue().repeatProperty());
        repeatColumn.setMinWidth(212);
        tasks = TaskAdapter.convertTaskListToObservable(DataBase.getTasks());
        Main.getPrimaryStage().setOnCloseRequest(we -> {
            tasks.forEach(TaskAdapter::stop);
            DataBase.exit();

        });
        tasksTable.setItems(tasks);
        tasksTable.setRowFactory(tv -> {
            TableRow<TaskAdapter> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (!row.isEmpty() && event.getButton() == MouseButton.PRIMARY
                        && event.getClickCount() == 1) {
                    TaskAdapter clickedRow = row.getItem();
                    showTaskWindow(clickedRow);
                }
            });
            return row;
        });
        tasks.addListener((ListChangeListener<TaskAdapter>) c -> {
            if (Main.getController().calendarIsShown())
                Platform.runLater(this::handleCalendar);
        });
    }

    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param main
     */
    public void setMainApp(Main main) {
        this.main = main;
    }




    private void createTaskWindow() {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/Task.fxml"));
        AnchorPane anchorPane = null;
        try {
            anchorPane = loader.load();
            taskController = loader.getController();
            Scene scene = new Scene(anchorPane);
            taskStage = new Stage();
            taskStage.setScene(scene);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showTaskWindow(TaskAdapter taskAdapter) {
        if (taskController != null && taskStage != null) {
            taskController.setTask(taskAdapter);
            taskController.setStage(taskStage);
            taskStage.show();
            taskStage.requestFocus();
        } else {
            createTaskWindow();
            showTaskWindow(taskAdapter);
        }
    }

    private void createCalendarWindow() {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/Calendar.fxml"));
        BorderPane borderPane = null;
        try {
            borderPane = loader.load();
            calendarController = loader.getController();
            Scene scene = new Scene(borderPane);
            calendarStage = new Stage();
            calendarStage.setScene(scene);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showCalendarWindow() {
        if (calendarController != null && calendarStage != null) {
            try {
                Date start = Task.DATE_FORMAT.parse(startField.getText());
                Date end = Task.DATE_FORMAT.parse(endField.getText());
                calendarController.set(Main.getController(), tasks, start, end);
                calendarController.setStage(taskStage);
                calendarStage.show();
                calendarStage.requestFocus();
            } catch (IllegalArgumentException e) {
                Common.showError(e.getMessage());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else {
            createCalendarWindow();
            showCalendarWindow();
        }
    }

    @FXML
    public void startTimePicker(){
        Date initValue = null;
        String text = startField.getText();
        if (!text.isEmpty())
            try {
                initValue = Task.DATE_FORMAT.parse(text);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        Common.showCalendarPicker(initValue, (ov, t, t1) -> {
            if (t1 == null)
                return;
            startField.setText(Task.DATE_FORMAT.format(t1.getTime()));
        });
    }
    @FXML
    public void endTimePicker(){
        Common.showCalendarPicker(null, (ov, t, t1) -> {
            if (t1 == null)
                return;
            endField.setText(Task.DATE_FORMAT.format(t1.getTime()));
        });
    }

    @FXML
    public void handleCalendar() {
        if (startField.getText().isEmpty() || endField.getText().isEmpty())
            Common.showError("No time interval is set!");
        else
            showCalendarWindow();
    }

    @FXML
    public void handleNewTask() {
        showTaskWindow(null);
    }

    public boolean calendarIsShown() {
        return calendarStage != null && calendarStage.isShowing();
    }

    public void addTask(String title, Date startTime, Date endTime, int repeat, boolean active){
        if (tasks.size() == 500) {
            Common.showError("C'mon! It's enough tasks!");
            return;
        }
        TaskAdapter taskAdapter = new TaskAdapter(title, startTime, endTime, repeat, active);
        tasks.add(taskAdapter);
        DataBase.addTask(taskAdapter.getTask());
    }

    public void removeTask(TaskAdapter taskAdapter) {
        tasks.remove(taskAdapter);
        DataBase.removeTask(taskAdapter.getTask());
    }
}

