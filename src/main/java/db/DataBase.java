package db;

import model.Task;
import org.apache.log4j.Logger;
import org.apache.log4j.pattern.LogEvent;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class DataBase {
    private final static Logger logger = Logger.getLogger(DataBase.class.getName());
    private static Connection connection;
    private static ExecutorService executor = Executors.newCachedThreadPool();

    private static String insertQuery = "INSERT INTO tasks "
            + "            (id, "
            + "             title, "
            + "             start, "
            + "             end, "
            + "             repeat, "
            + "             active) "
            + "VALUES      (?, ?, ?, ?, ?, ?)";

    public static void makeConnection(){
        try {
            Class.forName("org.h2.Driver");
            connection = DriverManager.getConnection("jdbc:h2:~/embedded", "embedded", "" );
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

    }

    public static List<Task> getTasks() {
        if (connection == null)
            makeConnection();
        createTable();
        ArrayList<Task> tasks = new ArrayList<>();
        try
        {
            Statement statement = connection.createStatement();

            ResultSet rs = statement.executeQuery("SELECT * FROM tasks");

            while(rs.next()) {
                int id = rs.getInt("id");
                String title = rs.getString("title");
                Date start = new Date(rs.getTimestamp("start").getTime());
                Date end = new Date(rs.getTimestamp("end").getTime());
                int repeat = rs.getInt("repeat");
                boolean active = rs.getBoolean("active");
                Task task = new Task(id, title, start, end, repeat);
                task.setActive(active);
                tasks.add(task);
            }
            statement.close();
        }  catch(Exception e) {
            System.out.println( e.getMessage() );
        }
        return tasks;
    }

    public static void addTask(Task task) {
        logger.debug("Adding task " + task.getTitle() + " [id=" + task.getId() + "]");
        executor.submit(() -> {
            if (connection == null)
                makeConnection();
            try {
                PreparedStatement preparedStatement = connection.prepareStatement(insertQuery);
                preparedStatement.setInt(1, task.getId());
                preparedStatement.setString(2, task.getTitle());
                preparedStatement.setTimestamp(3, new Timestamp(task.getStartTime().getTime()));
                preparedStatement.setTimestamp(4, new Timestamp(task.getEndTime().getTime()));
                preparedStatement.setInt(5, task.getRepeatInterval());
                preparedStatement.setBoolean(6, task.isActive());
                preparedStatement.executeUpdate();
                preparedStatement.close();
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        });
        logger.debug("Task " + task.getTitle() + " [id=" + task.getId() + "] added");
    }

    public static void createTable(){
        if (connection == null)
            makeConnection();
        try {
            Statement statement = connection.createStatement();
            String createSQL = ""
                    + "CREATE TABLE tasks "
                    + "  ( "
                    + "     id     BIGINT, "
                    + "     title  VARCHAR(5000), "
                    + "     start  TIMESTAMP, "
                    + "     end    TIMESTAMP, "
                    + "     repeat INT, "
                    + "     active BOOLEAN, " +
                    "PRIMARY KEY(id) "
                    + "  )";
            statement.executeUpdate(createSQL);
            logger.info("Table was created");
        } catch (SQLException e) {
            logger.info("Table exists");
        }
    }

    public static void exit() {
        logger.debug("Exit");
        closeConnection();
        try {
            executor.shutdown();
            executor.awaitTermination(1, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            logger.error(e.getMessage(), e);
        }
    }

    public static void closeConnection() {
        logger.debug("Closing connection");
        try {
            connection.close();
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
        }
        logger.debug("Connection closed");
    }

    public static void removeTask(Task task) {
        logger.debug("Removing task " + task.getTitle() + " [id=" + task.getId() + "]");
        executor.submit(() -> {
            if (connection == null)
                makeConnection();
            try {

                String removeQuery = "DELETE FROM tasks WHERE id = ?";
                PreparedStatement preparedStatement = connection.prepareStatement(removeQuery);
                preparedStatement.setInt(1, task.getId());
                preparedStatement.executeUpdate();
                preparedStatement.close();
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        });
        logger.debug("Task " + task.getTitle() + " [id=" + task.getId() + "] removed");
    }

    public static void updateTime(Task task){
        logger.debug("Updating task time" + task.getTitle() + " [id=" + task.getId() + "] - " + task.getTime());
        executor.submit(() -> {
            try {
                if (connection == null)
                    makeConnection();

                String updateSQL = "UPDATE tasks SET start = ?, end = ?, repeat = ? WHERE id = ?";

                PreparedStatement preparedStatement = connection.prepareStatement(updateSQL);
                preparedStatement.setTimestamp(1, new Timestamp(task.getStartTime().getTime()));
                preparedStatement.setTimestamp(2, new Timestamp(task.getEndTime().getTime()));
                preparedStatement.setInt(3, task.getRepeatInterval());
                preparedStatement.setInt(4, task.getId());
                preparedStatement.executeUpdate();
                preparedStatement.close();
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
            logger.debug("Task " + task.getTitle() + " [id=" + task.getId() + "] time updated");
        });
}

    public static void updateTitle(Task task) {
        logger.debug("Updating task title" + task.getTitle() + " [id=" + task.getId() + "]");
        executor.submit(() -> {
            if (connection == null)
                makeConnection();
            try {
                if (connection == null)
                    makeConnection();
                String updateSQL = "UPDATE tasks SET title = ? WHERE id = ?";
                PreparedStatement preparedStatement = connection.prepareStatement(updateSQL);
                preparedStatement.setString(1, task.getTitle());
                preparedStatement.setInt(2, task.getId());
                preparedStatement.executeUpdate();
                preparedStatement.close();
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        });
        logger.debug("Task " + task.getTitle() + " [id=" + task.getId() + "] title updated");
    }

    public static void updateActive(Task task){
        logger.debug("Updating task status" + task.getTitle() + " [id=" + task.getId() + "] - " + (task.isActive() ? "active" : "inactive"));
        executor.submit(() -> {
            if (connection == null)
                makeConnection();
            try {
                if (connection == null)
                    makeConnection();
                String updateSQL = "UPDATE tasks SET active = ? WHERE id = ?";
                PreparedStatement preparedStatement = connection.prepareStatement(updateSQL);
                preparedStatement.setBoolean(1, task.isActive());
                preparedStatement.setInt(2, task.getId());
                preparedStatement.executeUpdate();
                preparedStatement.close();
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        });
        logger.debug("Task " + task.getTitle() + " [id=" + task.getId() + "] set " + (task.isActive() ? "active" : "inactive"));
    }
}
