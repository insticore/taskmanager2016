package model;

import db.DataBase;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import job.ShowNotificationTask;

import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Created by Cyril on 14/01/2017.
 */
public class TaskAdapter {
    private final static Logger logger = Logger.getLogger(TaskAdapter.class.getName());
    private final Task task;
    private Timer timer = new Timer();
    private Alert notification;
    private static int maxId = 0;

    private StringProperty title  = new SimpleStringProperty();
    private BooleanProperty active = new SimpleBooleanProperty();
    private StringProperty start = new SimpleStringProperty();
    private StringProperty end = new SimpleStringProperty();
    private StringProperty repeat = new SimpleStringProperty();
    private BooleanProperty repeated = new SimpleBooleanProperty();

    public TaskAdapter(String title, Date startTime, Date endTime, int repeat, boolean active){
        this(new Task(++maxId, title, startTime, endTime, repeat));
        setActive(active);
    }

    public TaskAdapter(Task task) {
        validateTask(task.getTitle(), task.getStartTime(), task.getEndTime(), task.getRepeatInterval());
        this.task = task;
        setTitleProperty();
        setActiveProperty();
        setTimeProperties();
        notification = new Alert(Alert.AlertType.WARNING);
        notification.setHeaderText(null);
        notification.setTitle("Do it!");
        notification.setContentText(task.getTitle());
        if (task.getId() > maxId)
            maxId = task.getId();
    }

    public void set(String title, Date startTime, Date endTime, int repeat, boolean active){
        validateTask(title, startTime, endTime, repeat);
        setTitle(title);
        setTime(startTime, endTime, repeat);
        setActive(active);
    }

    public void validateTask (String title, Date startTime, Date endTime, int repeat){
        if (title == null || startTime == null || endTime == null) {
            logger.warning("null value");
            return;
        }
        if (title.isEmpty())
            throw new IllegalArgumentException("Title can't be empty. Sorry.");
        if (title.length() > 5000)
            throw new IllegalArgumentException("Too long title! Only up to 5000 characters title is permitted.");
        if ((endTime.getTime() - startTime.getTime() < repeat)
                || endTime.compareTo(startTime) < 0) {
            throw new IllegalArgumentException("Time can't be negative");
        }
        if (repeat < 0)
            throw new IllegalArgumentException("Repeat interval can't be negative");
    }

    public void setTime(Date startValue, Date endValue, int repeatValue) {
        {
            boolean update = false;
            if (!startValue.equals(task.getStartTime())) {
                if (repeatValue > 0)
                    task.setTime(startValue, endValue, repeatValue);
                else
                    task.setTime(startValue);
                update = true;
            } else if (!endValue.equals(task.getEndTime()) || repeatValue != task.getRepeatInterval()) {
                task.setTime(startValue, endValue, repeatValue);
                update = true;
            }
            if (update) {
                setTimeProperties();
                setTimer();
                DataBase.updateTime(task);
            }
        }
    }

    public void setTitle(String titleValue) {
        if (!titleValue.equals(task.getTitle())) {
            task.setTitle(titleValue);
            setTitleProperty();
            notification.setContentText(task.getTitle());
            DataBase.updateTitle(task);
        }
    }

    public void setActive(boolean activeValue) {
        if (activeValue != task.isActive()) {
            task.setActive(activeValue);
            setActiveProperty();
            setTimer();
            DataBase.updateActive(task);
        }
    }

    private void setTimeProperties() {
        start.set(Task.DATE_FORMAT.format(task.getStartTime()));
        end.set(Task.DATE_FORMAT.format(task.getEndTime()));
        repeat.set(task.formatRepeatInterval());
        if (task.getRepeatInterval() > 0)
            repeated.set(true);
        else
            repeated.set(false);
    }

    private void setTitleProperty() {
        title.set(task.getTitle());
    }

    private void setActiveProperty() {
        active.set(task.isActive());
    }

    public Task getTask() {
        return task;
    }

    public String getTitle() {
        return title.get();
    }

    public StringProperty titleProperty() {
        return title;
    }

    public boolean getActive() {
        return active.get();
    }

    public BooleanProperty activeProperty() {
        return active;
    }

    public String getStart() {
        return start.get();
    }

    public StringProperty startProperty() {
        return start;
    }

    public String getEnd() {
        return end.get();
    }

    public StringProperty endProperty() {
        return end;
    }

    public String getRepeat() {
        return repeat.get();
    }

    public StringProperty repeatProperty() {
        return repeat;
    }

    public boolean isRepeated() {
        return repeated.get();
    }

    public BooleanProperty repeatedProperty() {
        return repeated;
    }

    public void setRepeated(boolean repeated) {
        this.repeated.set(repeated);
        if (!repeated)
            setTime(task.getStartTime(), task.getEndTime(), 0);
    }

    protected void setTimer(){
        timer.cancel();
        if (task.isActive()) {
            timer = new Timer();
            Date nextTime = task.nextTimeAfter(new Date());
            if (nextTime == null)
                throw new IllegalArgumentException("You can't activate outdated task!");
            if (isRepeated()) {
                timer.schedule(new ShowNotificationTask(notification), task.nextTimeAfter(new Date()), task.getRepeatInterval() * 1000);
            } else {
                timer.schedule(new ShowNotificationTask(notification), task.nextTimeAfter(new Date()));
            }
        }
    }

    public void stop(){
        timer.cancel();
    }

    public static ObservableList<TaskAdapter> convertTaskListToObservable(List<Task> taskList){
        ObservableList<TaskAdapter> tasks = FXCollections.observableArrayList();
        tasks.addAll(taskList.stream().map(TaskAdapter::new).collect(Collectors.toList()));
        return tasks;
    }

    public static List<DateTaskEntry> calendar(Iterable<TaskAdapter> tasks, Date start, Date end)
            throws IllegalArgumentException {
        if (start.compareTo(end) <=0) {
            List<DateTaskEntry> resultList = new ArrayList<>();
            for (TaskAdapter taskAdapter : tasks) {
                Task task = taskAdapter.getTask();
                Date date = task.nextTimeAfter(start);
                while (date != null && date.compareTo(end) <= 0) {
                    DateTaskEntry entry = new DateTaskEntry(date, taskAdapter);
                    resultList.add(entry);
                    date = task.nextTimeAfter(date);
                }
            }
            Collections.sort(resultList, (o1, o2) -> o2.getDate().compareTo(o1.getDate()));
            return resultList;
        } else {
            throw new IllegalArgumentException("Start time can't be greater than end time");
        }
    }
}
