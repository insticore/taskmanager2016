package model;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Task is the class which encapsulates the state information
 * needed for the task and possible actions with that users can do with it.
 * This state information includes:
 * <ul>
 *      <li>The task title
 *      <li>The time of the task to be done
 *      <li>The task state of active or non-active
 *      <li>The start time for the repeated task
 *      <li>The end time for the repeated task
 *      <li>The interval time for the repeated task
 * </ul>
 * @author  Kyrylo Zimokos
 * @since   1.8
 */
public class Task implements Serializable {
    private final int id;
    private String title;
    private boolean active;
    private Date time;
    private Date start;
    private Date end;
    private int repeat;
    public final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("[yyyy-MM-dd HH:mm:ss.SSS]");

    /**
     * Constructor for not repeated task
     * @param title - title of not repeated task
     * @param time - time for not repeated task
     */
    public Task(int id, String title, Date time) {
        this.id = id;
        setActive(false);
        setTitle(title);
        setTime(time);
    }
    /**
     * Constructor for repeated task
     * @param title title of repeated task
     * @param start start time of repetition
     * @param end end time of repetition
     * @param repeat time period before repeat
     */
    public Task(int id, String title, Date start, Date end, int repeat) {
        this.id = id;
        setActive(false);
        setTitle(title);
        setTime(start, end, repeat);
    }
    /**
     * The method of obtaining the title of the problem
     * @return the title of the task
     */
    public String getTitle() {
        return title;
    }
    /**
     * The method of setting the header problem
     * @param title the title for the task
     */
    public void setTitle(String title) {
        if (!textHasContent(title)) {
            throw new IllegalArgumentException("Title can't be empty. Sorry.");
        }
        this.title = title;
    }
    /**
     * A method for checking the status of the task
     * @return true if the task is active or false if it's inactive
     */
    public boolean isActive() {
        return active;
    }
    /**
     * A method for setting the status of the task
     * @param active makes task active ot inactive
     */
    public void setActive(boolean active) {
        this.active = active;
    }
    /**
     * Method for setting time of notification of not repeated task
     * @param time time for notification about the task
     * @exception IllegalArgumentException when time parameter is negative
     */
    public void setTime(Date time) throws IllegalArgumentException {
        this.time = (Date) time.clone();
        start = (Date) time.clone();
        end = (Date) time.clone();
        repeat = 0;
    }
    /**
     * Method for setting time of notification of repeated task
     * @param start - time period before repeat
     * @param end - end time of repetition
     * @param repeat - time period before repeat
     * @exception IllegalArgumentException
     * when one of time paramaters is negative
     */
    public void setTime(Date start, Date end, int repeat)
            throws IllegalArgumentException {
        if ((end.getTime() - start.getTime() < repeat)
                || end.compareTo(start) < 0) {
            throw new IllegalArgumentException("Time can't be negative");
        }
        this.start = (Date)  start.clone();
        this.end = (Date)  end.clone();
        this.time =  (Date) start.clone();
        this.repeat = repeat;
    }
    /**
     * * Method for getting start time of notification of not repeated task
     * @return start time for repeated task or time for not repeated task
     */
    public Date getTime() {
        if (isRepeated()) {
            return (Date) start.clone();
        } else {
            return (Date) time.clone();
        }
    }
    /**
     * * Method for getting start time of notification
     * @return start time for repeated task or time for not repeated task
     */
    public Date getStartTime() {
        if (isRepeated()) {
            return (Date) start.clone();
        } else {
            return (Date) time.clone();
        }
    }
    /**
     * Method for getting end time of notification
     * @return end time for repeated task or time for not repeated task
     */
    public Date getEndTime() {
        if (isRepeated()) {
            return (Date) end.clone();
        } else {
            return (Date) time.clone();
        }
    }
    /**
     * @return interval between repetitions
     */
    public int getRepeatInterval() {
        return repeat;
    }
    /**
     * @return true if the task is repeated and false if it is not
     */
    public boolean isRepeated() {
        return repeat != 0;
    }
    /**
     * Method that forms a string to describe a task
     * @return string describing the task
     */
    public String toString() {
        StringBuffer wholeText = new StringBuffer();
        wholeText.append("\"").append(title).append("\"");
        if (isRepeated()) {
            String fromDateString = DATE_FORMAT.format(start);
            String toDateString = DATE_FORMAT.format(end);
            String intervalString = formatRepeatInterval();
            wholeText.append(" from ")
                    .append(fromDateString)
                    .append(" to ")
                    .append(toDateString)
                    .append(" every ")
                    .append(intervalString);
        } else {
            String timeString = DATE_FORMAT.format(time);
            wholeText.append(" at ")
                    .append(timeString);
        }
        if (!isActive()) {
            wholeText.append(" inactive");
        }
        return wholeText.toString();
    }
    /**
     * Method that returns time of the next notification after a specified time
     * @param time specify time, after which next repeat is calculated
     * @return time of next repetition after specified time
     */
    public Date nextTimeAfter(Date time) {
        if (!isActive()) {
            return null;
        }
        if (!isRepeated()) {
            if (time.compareTo(this.time) >= 0) {
                return null;
            } else {
                return getTime();
            }
        }
        if (time.compareTo(start) < 0) {
            return getStartTime();
        }
        Date result = (Date) start.clone();
        while (result.compareTo(time) <= 0) {
            result.setTime(result.getTime() + repeat * 1000);
        }
        if (result.compareTo(end) > 0) {
            return null;
        }
        return (Date) result.clone();
    }

    /**
     * Method that helps to validate input data
     * @return true if the string has text or false if it does not
     */
    private boolean textHasContent(String text) {
        return (text != null) && (!text.isEmpty());
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        Task task = (Task) obj;
        return this.getTitle().equals(task.getTitle())
                && this.time.equals(task.getTime())
                && this.start.equals(task.getStartTime())
                && this.end.equals(task.getEndTime())
                && this.repeat == task.getRepeatInterval()
                && this.active == task.isActive();

    }

    @Override
    public int hashCode() {
        int result = getTitle().hashCode();
        result = 31 * result + (isActive() ? 1 : 0);
        result = 31 * result + (getTime() != null ? getTime().hashCode() : 0);
        result = 31 * result + (start != null ? start.hashCode() : 0);
        result = 31 * result + (end != null ? end.hashCode() : 0);
        result = 31 * result + repeat;
        return result;
    }

    private void writeObject(ObjectOutputStream o)
            throws IOException {

        o.writeInt(title.length());
        o.writeUTF(title);
        o.writeInt(active ? 1 : 0);
        o.writeInt(repeat);
        if (isRepeated()) {
            o.writeObject(start);
            o.writeObject(end);
        } else {
            o.writeObject(time);
        }
    }

    private void readObject(ObjectInputStream o)
            throws IOException, ClassNotFoundException {

        o.readInt();
        String title = o.readUTF();
        setTitle(title);
        int active = o.readInt();
        boolean isActive = active > 0;
        setActive(isActive);
        repeat = o.readInt();
        if (repeat > 0) {
            Date start = (Date) o.readObject();
            Date end = (Date) o.readObject();
            setTime(start, end, repeat);
        } else {
            Date time = (Date) o.readObject();
            setTime(time);
        }
    }

    public String formatRepeatInterval() {
       int[] intervals = getRepeatIntervals();

        String daysString = intervals[0] > 1 ? "days" : "day";
        String hoursString = intervals[1] > 1 ? "hours" : "hour";
        String minutesString = intervals[2] > 1 ? "minutes" : "minute";
        String secondsString = intervals[3] > 1 ? "seconds" : "second";

        return "[" +
                intervals[0] + " " + daysString + " " +
                intervals[1] + " " + hoursString + " " +
                intervals[2] + " " + minutesString + " " +
                intervals[3] + " " + secondsString + "]";
    }

    public int[] getRepeatIntervals(){
        int[] intervals = new int[4];
        long interval = (long) repeat;
        long days = TimeUnit.SECONDS.toDays(interval);
        interval -= TimeUnit.DAYS.toSeconds(days);
        long hours = TimeUnit.SECONDS.toHours(interval);
        interval -= TimeUnit.HOURS.toSeconds(hours);
        long minutes = TimeUnit.SECONDS.toMinutes(interval);
        interval -= TimeUnit.MINUTES.toSeconds(minutes);
        long seconds = interval;

        intervals[0] = (int) days;
        intervals[1] = (int) hours;
        intervals[2] = (int) minutes;
        intervals[3] = (int) seconds;
        return intervals;
    }

    public int getId() {
        return id;
    }
}

