package model;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.StringProperty;

import java.util.Date;

public class DateTaskEntry {
    private ObjectProperty<Date> date = new SimpleObjectProperty<>();
    private TaskAdapter taskAdapter;

    public DateTaskEntry(Date date, TaskAdapter taskAdapter) {
        this.date.setValue(date);
        this.taskAdapter = taskAdapter;
    }

    public Date getDate() {
        return date.get();
    }

    public ObjectProperty<Date> dateProperty() {
        return date;
    }

    public void setDate(Date date) {
        this.date.set(date);
    }

    public TaskAdapter getTaskAdapter() {
        return taskAdapter;
    }

    public void setTaskAdapter(TaskAdapter taskAdapter) {
        this.taskAdapter = taskAdapter;
    }
}
